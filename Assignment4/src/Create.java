/**
 * Written by Phillip Hall, June 2014
 * Simple programs that makes and displays
 * three kinds of houses
 */
public class Create {

	/**Creates three different house objects and calls method from House.java to print them out
	 * 
	 * 
	 */
	public static void main(String[] args) {
		House myFirstHouse = new House("Yellow", "Hardwood", 0 );
		myFirstHouse.printHomeInfo();
		House mySecondHouse = new House("Purple", "Tiled", 0 );
		mySecondHouse.printHomeInfo();
		House myThirdHouse = new House("White", "Carpeted", 3 );
		myThirdHouse.printHomeInfo();
	}

}
