package world;

import javax.swing.*;

public class guiWindow {

	public guiWindow() {
		JFrame frame = new JFrame();
		frame.setTitle("Final Project Game");
		frame.setVisible(true);
		frame.setSize(600, 400);
		frame.add(new gameBoard());
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		new guiWindow();
	}
	
}
