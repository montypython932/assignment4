package world;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class gameBoard extends JPanel implements ActionListener {
	
	private Timer timer;
	
	public gameBoard() {
		timer = new Timer(25, this);
		timer.start();
	}

	public void actionPerformed(ActionEvent e){
		repaint();
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.white);
		g.fillRect(0, 0, 600, 400);
	}
	
	public static void main(String[] args) {
		

	}


}
